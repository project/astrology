<?php

declare(strict_types=1);

namespace Drupal\Tests\astrology\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group astrology
 */
final class AstrologyConfigTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['astrology', 'text', 'file'];

  /**
   * Test Default astrology table and configurations.
   */
  public function testDefaultAstrology(): void {
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/config/astrology/list');
    $assert_session = $this->assertSession();

    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('Listing astrology and configuration');

    // Verify that default astrology Zodiac show up.
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[1]/td[1]', 'Zodiac');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[1]/td[3]', 'Yes');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[1]/td[4]', 'EditSignsText');

    // Verify that expected value appears on configuration page.
    $astrologyForm = $assert_session->elementExists('css', '#astrology-config');
    $assert_session->selectExists('Format', $astrologyForm);
    $assert_session->selectExists('Astrology', $astrologyForm);
    $assert_session->fieldValueEquals('Format', 'day', $astrologyForm);
    $assert_session->fieldValueEquals('Astrology', '1', $astrologyForm);
    $assert_session->checkboxChecked('Sign information block', $astrologyForm);

    $astrologyForm->submit();
    $assert_session->pageTextContains('The Zodiac has been set as default, and data will be shown per day');

    // Update format character to show as week.
    $astrologyForm->fillField('Format', 'week');
    $astrologyForm->submit();
    $assert_session->pageTextContains('The Zodiac has been set as default, and data will be shown per week');

    // Update format character to show as month.
    $astrologyForm->fillField('Format', 'month');
    $astrologyForm->submit();
    $assert_session->pageTextContains('The Zodiac has been set as default, and data will be shown per month');

    // Update format character to show as year.
    $astrologyForm->fillField('Format', 'year');
    $astrologyForm->submit();
    $assert_session->pageTextContains('The Zodiac has been set as default, and data will be shown per year');

    // Visit default astrology sign page and assert that all 12 sign exists.
    $this->drupalGet('admin/config/astrology/list/1/signs');
    $sign = [
      'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra',
      'Scorpio', 'Sagittarius', 'Capricorn', 'Aquarius', 'Pisces',
    ];
    foreach ($sign as $key => $name) {
      $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[' . ($key + 1) . ']/td[2]', $name);
    }

    // Assert that default sign name are disabled.
    $this->drupalGet('admin/config/astrology/list/1/signs/1/edit');
    $assert_session->fieldDisabled('Name');
    $assert_session->fieldDisabled('from_date_month');
    $assert_session->fieldDisabled('from_date_day');
    $assert_session->fieldDisabled('to_date_month');
    $assert_session->fieldDisabled('to_date_day');

    // Assert that default sign can't be deleted.
    $this->drupalGet('admin/config/astrology/list/1/signs/1/delete');
    $assert_session->statusCodeEquals(403);
    $assert_session->pageTextContains('This sign can not be deleted');

    // Add text for aries sign.
    $this->drupalGet('admin/config/astrology/list/1/signs/1/text');
    $this->submitForm([
      'Select Day' => '28-07-2024',
      'Enter Text' => 'Test text for Aries sign.',
    ], 'Save');
    $assert_session->pageTextContains('Text added for the day Sunday 28 July.');

    // Verify that updated text message appear
    // if user enters text for same date.
    $this->submitForm([
      'Select Day' => '28-07-2024',
      'Enter Text' => 'Update test text for test sign.',
    ], 'Save');
    $assert_session->pageTextContains('Text updated for the day Sunday 28 July.');

    // Verify that added text appears on search page
    // for aries sign for dated '28-07-2024'.
    $this->drupalGet('admin/config/astrology/list/1/text');
    $this->submitForm([
      // sign_id of Aries.
      'Sign' => '1',
      'Day' => '28-07-2024',
    ], 'Search');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr/td[2]', 'Aries');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr/td[4]', '28 Jul,2024');
    $assert_session->linkExists('Edit');

    // Edit text from Search page and make the update.
    $this->clickLink('Edit');
    $this->submitForm([
      'Enter Text' => 'Update text for aries sign from search page.',
    ], 'Update');
    $assert_session->pageTextContains('Text updated.');
  }

  /**
   * Test Default astrology table and configurations.
   */
  public function testAddUpdateDeleteAstrology(): void {
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);

    $this->drupalGet('admin/config/astrology/add');
    $testAstrology = [
      'Name' => 'Test astrology',
      'Enabled' => 0,
      'Description' => 'Test description',
    ];
    $this->submitForm($testAstrology, 'Save');

    $assert_session = $this->assertSession();
    $assert_session->pageTextContains('Astrology Test astrology created');

    // Edit the above crated astrology and make as default astrology.
    $this->drupalGet('admin/config/astrology/2/edit');
    $testAstrology['Enabled'] = 1;
    $this->submitForm($testAstrology, 'Update');
    $assert_session->pageTextContains('Astrology Test astrology updated');

    $this->drupalGet('admin/config/astrology/list');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[2]/td[1]', 'Test astrology');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[2]/td[3]', 'Yes');

    // Verify Zodiac is not a default astrology anymore.
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[1]/td[1]', 'Zodiac');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[1]/td[3]', 'No');

    // Visit sign page and create one test sign.
    $this->drupalGet('admin/config/astrology/list/2/signs');
    $assert_session->statusCodeEquals(200);

    // Add sign under test astrology.
    $this->drupalGet('admin/config/astrology/list/2/signs/add');
    $signData = [
      'Name' => 'Test sign',
      'Description' => 'Test sign description',
    // January.
      'from_date_month' => 1,
    // 1st
      'from_date_day' => 1,
    // March.
      'to_date_month' => 3,
    // 23rd
      'to_date_day' => 23,
    ];
    $this->submitForm($signData, 'Save');
    $assert_session->pageTextContains('Sign Test sign added.');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[1]/td[2]', 'Test sign');

    // Verify sign edit page.
    $this->drupalGet('admin/config/astrology/list/2/signs/13/edit');
    $this->submitForm($signData, 'Save');
    $assert_session->pageTextContains('Sign Test sign updated.');

    // Add text for created sign.
    $this->drupalGet('admin/config/astrology/list/2/signs/13/text');
    $this->submitForm([
      'Select Day' => '28-07-2024',
      'Enter Text' => 'Test text for test sign.',
    ], 'Save');
    $assert_session->pageTextContains('Text added for the day Sunday 28 July.');

    // Verify that updated text message appear
    // if user enters text for same date.
    $this->submitForm([
      'Select Day' => '28-07-2024',
      'Enter Text' => 'Update test text for test sign.',
    ], 'Save');
    $assert_session->pageTextContains('Text updated for the day Sunday 28 July.');

    // Verify that added text appears for test sign for that day.
    $this->drupalGet('admin/config/astrology/list/2/text');
    $this->submitForm([
    // Test sign sign_id.
      'Sign' => '13',
      'Day' => '28-07-2024',
    ], 'Search');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr/td[2]', 'Test sign');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr/td[4]', '28 Jul,2024');
    $assert_session->linkExists('Edit');

    // Edit text from Search page and make the update.
    $this->clickLink('Edit');
    $this->submitForm([
      'Enter Text' => 'Update text for test sign from search page.',
    ], 'Update');
    $assert_session->pageTextContains('Text updated.');

    // Delete the above created astrology and
    // assert that default astrology set back to Zodiac.
    $this->drupalGet('admin/config/astrology/2/delete');
    $this->submitForm([], 'Delete');
    $assert_session->elementNotExists('xpath', '//table[contains(@class,"responsive-enabled")]//tr[2]');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[1]/td[1]', 'Zodiac');
    $assert_session->elementTextEquals('xpath', '//table[contains(@class,"responsive-enabled")]//tr[1]/td[3]', 'Yes');
  }

}
