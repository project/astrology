<?php

declare(strict_types=1);

namespace Drupal\Tests\astrology\Kernel;

use Drupal\astrology\Services\AstrologyUtilityService;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test description.
 *
 * @group astrology
 */
final class AstrologyUtilityServiceTest extends KernelTestBase {

  /**
   * The AstrologyUtilityService object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['astrology'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->astrologyUtilityService = new AstrologyUtilityService(
      $this->container->get('config.factory')
    );
  }

  /**
   * Test astrology check for valid date.
   *
   * @dataProvider astrologyCheckNextPrevDataProvider
   */
  public function testAstrologyCheckValidDate($formatter, $nextPrev, $expected): void {
    $this->assertTrue($this->astrologyUtilityService->astrologyCheckValidDate($formatter, $nextPrev));
  }

  /**
   * Test next previous functionality.
   *
   * @dataProvider astrologyCheckNextPrevDataProvider
   */
  public function testAstrologyCheckNextPrev($formatter, $nextPrev, $expected): void {
    self::assertSame(
      $expected,
      $this->astrologyUtilityService->astrologyCheckNextPrev($formatter, $nextPrev)
    );
  }

  /**
   * Data provider for testAstrologyCheckNextPrev, testAstrologyCheckValidDate.
   */
  public static function astrologyCheckNextPrevDataProvider(): array {
    $fix_date = strtotime('02/07/2024');
    return [
      ['day', (int) date('z', $fix_date), ['next' => 38, 'prev' => 36]],
      ['week', (int) date('W', $fix_date), ['next' => 7, 'prev' => 5]],
      ['month', (int) date('n', $fix_date), ['next' => 3, 'prev' => 1]],
      ['year', (int) date('o', $fix_date), ['next' => 2025, 'prev' => 2023]],
    ];
  }

  /**
   * Validate get days array.
   */
  public function testGetDaysArray() {
    $this->assertSame([
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
      6 => 6,
      7 => 7,
      8 => 8,
      9 => 9,
      10 => 10,
      11 => 11,
      12 => 12,
      13 => 13,
      14 => 14,
      15 => 15,
      16 => 16,
      17 => 17,
      18 => 18,
      19 => 19,
      20 => 20,
      21 => 21,
      22 => 22,
      23 => 23,
      24 => 24,
      25 => 25,
      26 => 26,
      27 => 27,
      28 => 28,
      29 => 29,
      30 => 30,
      31 => 31,
    ], $this->astrologyUtilityService->getDaysArray());
  }

  /**
   * Validate get years array.
   */
  public function testGetYearsArray() {
    $this->assertSame(array_combine(range(date("Y") - 1, date("Y") + 1), range(date("Y") - 1, date("Y") + 1)), $this->astrologyUtilityService->getYearsArray());
  }

}
