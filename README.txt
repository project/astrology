CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module creates a default astrology named "Zodiac", which consists 12
astrological signs, namely Aries, Taurus, Gemini, Cancer, Leo, Virgo, Libra,
Scorpio, Sagittarius, Capricorn, Aquarius and Pisces.

In order to use this module functionality, site administrator need to add text
for each sign and for each format.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/astrology

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/astrology


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/node/1897420
for further information


CONFIGURATION
-------------

There will be default configuration set at time of module installation, while
this can be changed as follows

    1. Configure astrology setting to display astrological data by navigating to
       Administration » Configuration » Astrology settings and under Site wide
       settings tab select 'format', 'astrology' and 'sign information block'.
    2. Administration activities like adding text for day, month and year go to
       Administration » Configuration » Astrology settings and under
       Administer settings tab set 'format'.
    3. Configure block 'astrology' by placing it to your site region navigate to
       Administration » Structure » Block layout click on Place block bottom to
       add block.
    4. Custom astrology can be created if default(Zodiac) doesn't fit to the
       requirements by clicking on "add astrology" button, there will be links
       to add sing, list text, edit, delete etc.
    5. Every astrology should have "star sign" like zodiac has Aries, Taurus
       etc., which can be seen by clicking on "list sign" button
    6. Text can be added for every sign, according to the format currently
       selected from #2


MAINTAINERS
-----------

Current maintainer:

 * Chandan Singh (chandu7929) - https://www.drupal.org/u/chandu7929
