<?php

namespace Drupal\astrology\Plugin\Block;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'AstrologyBlock' block.
 *
 * @Block(
 *  id = "astrology",
 *  admin_label = @Translation("Astrology"),
 * )
 */
class AstrologyBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\Core\Session\AccountInterface.
   *
   * @var account\Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Constructor for this class.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AstrologyCoreService $astrology_core_service,
    ConfigFactoryInterface $config_factory,
    AccountInterface $account,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->astrologyCoreService = $astrology_core_service;
    $this->config = $config_factory;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('astrology.core'),
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    $is_admin = $this->account->hasPermission('Administrator');
    $astrology_config = $this->config->get('astrology.settings');
    $astrology_id = $astrology_config->get('astrology');

    $blank_msg = NULL;
    if ($is_admin) {
      $blank_msg = 'The default selected astrology does not contains any sign, please add one or more sign to display here.';
    }

    return [
      '#theme' => 'astrology',
      '#formatter' => $astrology_config->get('format_character'),
      '#signs' => $this->astrologyCoreService->getAstrologySigns($astrology_id),
      '#blank_msg' => $blank_msg,
      '#attached' => [
        'library' => [
          'astrology/astrology.module',
        ],
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    // Set cache tag for astrology block.
    return Cache::mergeTags(parent::getCacheTags(), ['astrology_block']);
  }

}
