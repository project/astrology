<?php

namespace Drupal\astrology\Services;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\StatementInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Astrology core services.
 */
class AstrologyCoreService {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $cacheTagsInvalidator;

  /**
   * Constructs a new AstrologyCoreService object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The database connection.
   * @param \Drupal\Core\Cache\CacheTagsInvalidator $cache_tags_invalidator
   *   The database connection.
   */
  public function __construct(
    Connection $connection,
    ConfigFactoryInterface $config_factory,
    CacheTagsInvalidator $cache_tags_invalidator,
  ) {
    $this->connection = $connection;
    $this->config = $config_factory;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * Add new astrology to the system.
   *
   * @param array $data
   *   The data for astrology.
   *
   * @return int|string|null
   *   Status of operations.
   *
   * @throws \Exception
   */
  public function addAstrology($data): int|string|null {
    return $this->connection->insert('astrology')->fields($data)->execute();
  }

  /**
   * Update astrology data.
   *
   * @param string $astrology_id
   *   The astrology ID.
   * @param array $data
   *   The data.
   *
   * @return int|null
   *   The status of update operations.
   */
  public function updateAstrology(string $astrology_id, array $data): ?int {
    return $this->connection->update('astrology')
      ->fields($data)
      ->condition('id', $astrology_id)
      ->execute();
  }

  /**
   * Get all available astrology.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   Astrology record.
   */
  public function getAllAstrology(): StatementInterface {
    return $this->connection->select('astrology', 'a')
      ->fields('a')
      ->execute();
  }

  /**
   * Returns associative array of astrology.
   */
  public function getAstrologyArray(): array {
    $data = [];
    $astrology = $this->getAllAstrology();
    while ($row = $astrology->fetchObject()) {
      $data[$row->id] = $row->name;
    }
    return $data;
  }

  /**
   * Get astrology data.
   *
   * @param string $astrology_id
   *   The astrology ID.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   The astrology data.
   */
  public function getAstrology(string $astrology_id): ?StatementInterface {
    $astrology = NULL;
    if ($this->hasAstrology($astrology_id)) {
      $astrology = $this->connection->select('astrology', 'a')
        ->fields('a')
        ->condition('id', $astrology_id)
        ->execute();
    }
    return $astrology;
  }

  /**
   * Disable all astrology.
   */
  public function disableAllAstrology(): void {
    $this->connection->update('astrology')
      ->fields([
        'enabled' => 0,
      ])->execute();
  }

  /**
   * Enable specific astrology.
   *
   * @param string $id
   *   The astrology ID.
   */
  public function enableAstrology(string $id): void {
    $this->connection->update('astrology')
      ->fields([
        'enabled' => 1,
      ])
      ->condition('id', $id)
      ->execute();

    // Invalidate astrology block cache on update astrology.
    $this->cacheTagsInvalidator->invalidateTags(['astrology_block']);
  }

  /**
   * Check if astrology ID exists.
   *
   * @param string $astrology_id
   *   The astrology ID.
   *
   * @return mixed
   *   Record count.
   */
  private function hasAstrology(string $astrology_id): mixed {
    return $this->connection->select('astrology', 'a')
      ->fields('a')
      ->condition('id', $astrology_id)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * Check give name exist with other astrology iD.
   *
   * @param string $astrology_name
   *   The astrology name.
   * @param string|null $astrology_id
   *   The astrology ID.
   *
   * @return mixed
   *   Status with duplicate name.
   */
  public function checkForDuplicateAstrologyName(string $astrology_name, string $astrology_id = NULL): mixed {
    $query = $this->connection->select('astrology', 'a')
      ->fields('a')
      ->condition('name', $astrology_name);
    if ($astrology_id) {
      $query->condition('id', $astrology_id, '<>');
    }
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Add astrology sign.
   *
   * @param array $data
   *   The astrological data.
   *
   * @throws \Exception
   */
  public function addAstrologySign(array $data): void {
    $this->connection->insert('astrology_signs')
      ->fields($data)
      ->execute();
  }

  /**
   * Update astrology sign.
   *
   * @param array $data
   *   The astrology data.
   * @param string $sign_id
   *   The sign ID.
   * @param string $astrology_id
   *   The astrology ID.
   */
  public function updateAstrologySign(array $data, string $sign_id, string $astrology_id): void {
    $this->connection->update('astrology_signs')
      ->fields($data)
      ->condition('id', $sign_id)
      ->condition('astrology_id', $astrology_id)
      ->execute();

    // Invalidate astrology block cache on update astrology.
    $this->cacheTagsInvalidator->invalidateTags(['astrology_block']);
  }

  /**
   * Get astrology signs.
   *
   * @param string $astrology_id
   *   The astrology id.
   * @param string|null $sign_name
   *   The sign name.
   * @param string|null $sign_id
   *   The sign name.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   The astrological sign data.
   */
  public function getAstrologySigns(string $astrology_id, string $sign_name = NULL, string $sign_id = NULL): ?StatementInterface {
    $signs = NULL;
    if ($this->astrologyHasSign($astrology_id)) {
      $query = $this->connection->select('astrology_signs', 's')
        ->fields('s')
        ->condition('s.astrology_id', $astrology_id);
      if ($sign_name) {
        $query->condition('name', $sign_name);
      }
      if ($sign_id) {
        $query->condition('id', $sign_id);
      }
      $signs = $query->execute();
    }
    return $signs;
  }

  /**
   * Return associative array of astrology.
   *
   * @param string $astrology_id
   *   The astrology ID.
   *
   * @return array
   *   The array of astrological signs.
   */
  public function getAstrologyListSignArray(string $astrology_id): array {
    $sign = $this->getAstrologySigns($astrology_id);
    $data = [];
    if ($sign) {
      $data[0] = 'ALL';
      while ($row = $sign->fetchObject()) {
        $data[$row->id] = $row->name;
      }
    }
    return $data;
  }

  /**
   * Check for duplicate sign name.
   *
   * @param string $sign_name
   *   The name of sign.
   * @param string|null $sign_id
   *   The sing ID.
   *
   * @return mixed
   *   Status of duplicate name existence.
   */
  public function checkForDuplicateSignName(string $sign_name, string $sign_id = NULL): mixed {
    $query = $this->connection->select('astrology_signs', 'a')
      ->fields('a')
      ->condition('name', $sign_name);
    if ($sign_id) {
      $query->condition('id', $sign_id, '<>');
    }
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get count of astrology sing.
   *
   * @param string $astrology_id
   *   The astrology id.
   *
   * @return mixed
   *   Count of record.
   */
  private function astrologyHasSign(string $astrology_id): mixed {
    return $this->connection->select('astrology_signs', 's')
      ->fields('s')
      ->condition('s.astrology_id', $astrology_id)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * Add text for particular sign.
   *
   * @param array $data
   *   The text data.
   *
   * @throws \Exception
   */
  public function astrologyAddSignText(array $data): void {
    $this->connection->insert('astrology_text')
      ->fields($data)
      ->execute();
  }

  /**
   * Update text for particular date and format.
   *
   * @param string $id
   *   The text ID.
   * @param array $data
   *   The text data.
   */
  public function astrologyUpdateSignText(string $id, array $data): void {
    $this->connection->update('astrology_text')
      ->fields($data)
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Get text for all signs.
   *
   * @param string $astrology_id
   *   The astrology ID.
   * @param string $format
   *   The format.
   * @param string $timestamp
   *   The timestamp.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   The text from all signs.
   */
  public function getAllTextForAllSigns(string $astrology_id, string $format, string $timestamp): ?StatementInterface {
    $subquery = $this->connection->select('astrology', 'h');
    $subquery->join('astrology_signs', 'hs', 'hs.astrology_id = h.id');
    $subquery->fields('hs', ['id'])->condition('hs.astrology_id', $astrology_id, '=');
    $query = $this->connection->select('astrology_text', 'ht');
    $query->join('astrology_signs', 'hs', 'hs.id = ht.astrology_sign_id');
    $query->join('astrology', 'h', 'hs.astrology_id = h.id');
    $query->fields('ht', [
      'id', 'text', 'text_format', 'value', 'astrology_sign_id', 'post_date',
    ]);
    $query->fields('hs', ['icon', 'name']);
    $query->fields('h', ['id'])
      ->condition('h.id', $astrology_id)
      ->condition('astrology_sign_id', $subquery, 'IN')
      ->condition('format_character', $format)
      ->condition('value', $timestamp);
    return $query->execute();
  }

  /**
   * Get text of particular sign.
   *
   * @param string $astrology_id
   *   The astrology ID.
   * @param string $astrology_sign
   *   The astrology sing ID.
   * @param string $format
   *   The format character.
   * @param string $timestamp
   *   The timestamp.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   The text for particular sing.
   */
  public function getAllTextForSign(string $astrology_id, string $astrology_sign, string $format, string $timestamp): ?StatementInterface {
    $query = $this->connection->select('astrology_text', 'ht');
    $query->join('astrology_signs', 'hs', 'hs.id = ht.astrology_sign_id');
    $query->join('astrology', 'h', 'hs.astrology_id = h.id');
    $query->fields('ht', [
      'id', 'text', 'text_format', 'value', 'astrology_sign_id', 'post_date',
    ]);
    $query->fields('hs', ['icon', 'name', 'astrology_id']);
    $query->fields('h', ['id']);
    $query->condition('h.id', $astrology_id)
      ->condition('hs.id', $astrology_sign)
      ->condition('ht.format_character', $format)
      ->condition('ht.value', $timestamp);
    return $query->execute();
  }

  /**
   * Get text for given sign id.
   *
   * @param string $sign_id
   *   The sign ID.
   * @param string $date
   *   The date.
   * @param string $format
   *   The format character.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   Text for geven sign.
   */
  public function getAstrologicalSignText(string $sign_id, string $date, string $format = 'z'): ?StatementInterface {
    $text = NULL;
    if ($this->checkSignTextExistence($sign_id, $date, $format)) {
      $text = $this->connection->select('astrology_text', 'h')
        ->fields('h')
        ->condition('value', $date)
        ->condition('astrology_sign_id', $sign_id)
        ->condition('format_character', $format)
        ->execute();
    }
    return $text;
  }

  /**
   * Check if text already exists for give match.
   *
   * @param string $astrology_sign_id
   *   The astrology sing ID.
   * @param string $date
   *   The date.
   * @param string $format_character
   *   The format character.
   *
   * @return mixed
   *   Record count.
   */
  private function checkSignTextExistence(string $astrology_sign_id, string $date, string $format_character): mixed {
    return $this->connection->select('astrology_text', 'at')
      ->fields('at')
      ->condition('astrology_sign_id', $astrology_sign_id)
      ->condition('format_character', $format_character)
      ->condition('value', $date)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * Update text for particular sign.
   *
   * @param array $data
   *   The data for text.
   * @param string $text_id
   *   The text ID.
   *
   * @return int|null
   *   Status of operations.
   */
  public function updateText(array $data, string $text_id): ?int {
    return $this->connection->update('astrology_text')
      ->fields($data)
      ->condition('id', $text_id,)
      ->execute();
  }

  /**
   * Return TRUE if text_id belongs to a valid sign.
   *
   * @param string $sign_id
   *   The sign ID.
   * @param string $text_id
   *   The text ID.
   *
   * @return mixed
   *   Text for give sing iD.
   */
  public function isValidText(string $sign_id, string $text_id) {
    $query = $this->connection->select('astrology_text', 'at_')
      ->fields('at_')
      ->condition('id', $text_id)
      ->condition('astrology_sign_id ', $sign_id)
      ->execute();
    $result = $query->fetchObject();
    if (!$result) {
      throw new AccessDeniedHttpException();
    }
    return $result;
  }

  /**
   * Delete all text of given sign.
   *
   * @param string $sign_id
   *   The sign ID.
   */
  public function deleteAllText(string $sign_id): void {
    $this->connection->delete('astrology_text')
      ->condition('astrology_sign_id', $sign_id)
      ->execute();
  }

  /**
   * Delete given sign.
   *
   * @param string $astrology_id
   *   The astrology ID.
   * @param string $sign_id
   *   The sing ID.
   */
  public function deleteSign(string $astrology_id, string $sign_id): void {
    $this->deleteAllText($sign_id);
    $this->connection->delete('astrology_signs')
      ->condition('id', $sign_id)
      ->condition('astrology_id', $astrology_id)
      ->execute();
  }

  /**
   * Deleted astrology.
   *
   * @param string $astrology_id
   *   The astrology ID.
   */
  public function deleteAstrology(string $astrology_id): void {
    $signs = $this->getAstrologySigns($astrology_id);
    if ($signs) {
      while ($sign = $signs->fetchObject()) {
        $this->deleteSign($astrology_id, $sign->id);
      }
    }
    $this->connection->delete('astrology')
      ->condition('id', $astrology_id)
      ->execute();
  }

  /**
   * Update default astrology if there is a change.
   *
   * @param string $astrology_id
   *   The astrology ID.
   * @param int $status
   *   Enable or disable.
   * @param string $op
   *   The operations.
   */
  public function updateDefaultAstrology(string $astrology_id, int $status, string $op): void {

    $astrologyConfig = $this->config->get('astrology.settings');
    $default_astrology = $astrologyConfig->get('astrology');

    if ($op == 'new' && $status) {
      $this->updateAstrologyConfigSettings('0', $default_astrology, $astrology_id);
    }
    elseif ($op == 'update' && $astrology_id != $default_astrology && $status) {
      $this->updateAstrologyConfigSettings(0, $default_astrology, $astrology_id);
    }
    elseif ($op == 'update' && $astrology_id == $default_astrology && !$status) {
      $this->updateAstrologyConfigSettings(1, '1', '1');
    }
    elseif ($op == 'delete' && $astrology_id == $default_astrology) {
      $this->updateAstrologyConfigSettings(1, '1', '1');
    }
  }

  /**
   * Update configurations, if changes made on astrology settings page.
   *
   * @param int $enabled
   *   The status.
   * @param string $default_astrology
   *   If its default astrology.
   * @param string $astrology_id
   *   The astrology ID.
   */
  private function updateAstrologyConfigSettings(int $enabled, string $default_astrology, string $astrology_id): void {

    $astrology_config = $this->config->getEditable('astrology.settings');
    $this->connection->update('astrology')
      ->fields([
        'enabled' => $enabled,
      ])->condition('id', $default_astrology)
      ->execute();

    $astrology_config->set('astrology', $astrology_id)->save();

    // Invalidate astrology block cache on update astrology.
    $this->cacheTagsInvalidator->invalidateTags(['astrology_block']);
  }

}
