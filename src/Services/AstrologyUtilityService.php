<?php

namespace Drupal\astrology\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Astrology utility services.
 */
class AstrologyUtilityService {

  use StringTranslationTrait;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $config;

  /**
   * Constructs a new utility service object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The database connection.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory;
  }

  /**
   * Returns first and last day of week.
   */
  public function getFirstLastDow($new_date = ''): array {
    // +1 for first day of week as Monday.
    $week_days[] = mktime(0, 0, 0, date("n", $new_date), date("j", $new_date) - date("N", $new_date) + 1);
    // +7 for last day of week as Sunday.
    $week_days[] = mktime(0, 0, 0, date("n", $new_date), date("j", $new_date) - date("N", $new_date) + 7);

    return $week_days;
  }

  /**
   * Check and return next and previous link accordingly.
   *
   * @param string $formatter
   *   The formatter.
   * @param int $next_prev
   *   The next and previous value.
   *
   * @return array
   *   the array with next prev key.
   */
  public function astrologyCheckNextPrev(string $formatter, int $next_prev): array {

    $next_prev_val = [];

    // If leap year, set number of days in year.
    $last_day_of_year = date('L') ? 365 : 364;

    // If leap year, set max week number in year.
    $last_week_of_year = date('L') ? 53 : 52;

    $last_month_of_year = 12;
    $max_year_range = date('Y') + 1;
    $min_year_range = date('Y') - 1;

    switch ($formatter) {
      case 'day':
        if ($next_prev < $last_day_of_year && $next_prev > 0) {
          $next_prev_val['next'] = $next_prev + 1;
          $next_prev_val['prev'] = ($next_prev - 1) ?: '+0';
        }
        elseif ($next_prev == $last_day_of_year) {
          $next_prev_val['next'] = FALSE;
          $next_prev_val['prev'] = $next_prev - 1;
        }
        elseif ($next_prev == '0') {
          $next_prev_val['next'] = 1;
          $next_prev_val['prev'] = FALSE;
        }
        break;

      case 'week':
        $this->nextPrevForWeekMonth($next_prev_val, $next_prev, $last_week_of_year);
        break;

      case 'month':
        $this->nextPrevForWeekMonth($next_prev_val, $next_prev, $last_month_of_year);
        break;

      case 'year':
        if ($next_prev > $min_year_range && $next_prev < $max_year_range) {
          $next_prev_val['next'] = $next_prev + 1;
          $next_prev_val['prev'] = $next_prev - 1;
        }
        elseif ($next_prev == $max_year_range) {
          $next_prev_val['next'] = FALSE;
          $next_prev_val['prev'] = $next_prev - 1;
        }
        elseif ($next_prev == $min_year_range) {
          $next_prev_val['next'] = $next_prev + 1;
          $next_prev_val['prev'] = FALSE;
        }
        break;
    }
    return $next_prev_val;
  }

  /**
   * Update next prev value for week and month.
   *
   * @param array $next_prev_val
   *   The next and previous value.
   * @param int $next_prev
   *   The next and prev.
   * @param string $last_date
   *   The last date.
   *
   * @return void
   *   The array with next prev.
   */
  private function nextPrevForWeekMonth(array &$next_prev_val, int $next_prev, string $last_date): void {
    if ($next_prev >= 1 && $next_prev < $last_date) {
      $next_prev_val['next'] = $next_prev + 1;
      $next_prev_val['prev'] = $next_prev - 1;
    }
    elseif ($next_prev == $last_date) {
      $next_prev_val['next'] = FALSE;
      $next_prev_val['prev'] = $next_prev - 1;
    }
  }

  /**
   * Checks for valid formatter and next_prev value supplied.
   *
   * @param string $formatter
   *   The formatter.
   * @param int $next_prev
   *   The next and previous.
   *
   * @return bool
   *   Boolean if the date is valid.
   */
  public function astrologyCheckValidDate(string $formatter, int $next_prev): bool {

    // If leap year, set number of days in year.
    $last_day_of_year = date('L') ? 365 : 364;

    // If leap year, set max week number in year.
    $last_week_of_year = date('L') ? 53 : 52;

    $last_month_of_year = 12;
    $max_year_range = date('Y') + 1;
    $min_year_range = date('Y') - 1;

    if ($formatter == 'day' && ($next_prev < 0 || $next_prev > $last_day_of_year)) {
      return FALSE;
    }
    if ($formatter == 'week' && ($next_prev < 1 || $next_prev > $last_week_of_year)) {
      return FALSE;
    }
    if ($formatter == 'month' && ($next_prev < 1 || $next_prev > $last_month_of_year)) {
      return FALSE;
    }
    if ($formatter == 'year' && ($next_prev < $min_year_range || $next_prev > $max_year_range)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns array of months.
   */
  public function getMonthsArray(): array {
    return [
      1 => $this->t('January'),
      2 => $this->t('February'),
      3 => $this->t('March'),
      4 => $this->t('April'),
      5 => $this->t('May'),
      6 => $this->t('June'),
      7 => $this->t('July'),
      8 => $this->t('August'),
      9 => $this->t('September'),
      10 => $this->t('October'),
      11 => $this->t('November'),
      12 => $this->t('December'),
    ];
  }

  /**
   * Returns associative array of days.
   */
  public function getDaysArray(): array {
    $days_array = [];
    $i = 1;
    while ($i <= 31) {
      $days_array[$i] = $i;
      $i += 1;
    }
    return $days_array;
  }

  /**
   * Returns array of years, next 10 years.
   */
  public function getYearsArray(): array {
    return array_combine(range(date("Y") - 1, date("Y") + 1), range(date("Y") - 1, date("Y") + 1));
  }

  /**
   * Returns timestamps for the given date.
   *
   * @param string $date
   *   The date value.
   *
   * @return bool|int
   *   The tiemestamp of given date or false.
   */
  public function getTimestamps(string $date = ''): bool|int {
    return strtotime($date);
  }

  /**
   * Returns formatted date value.
   *
   * @param string $formatter
   *   The formatter.
   * @param string $new_date
   *   The new date value.
   *
   * @return string
   *   The new date string.
   */
  public function getFormatDateValue(string $formatter, string $new_date): string {
    $timestamps = strtotime($new_date);
    $month = date('m', $timestamps);
    $day = date('d', $timestamps);
    return date($formatter, mktime(0, 0, 0, $month, $day));
  }

  /**
   * Returns day of the year.
   *
   * @param string $day
   *   The day value.
   * @param string $format
   *   The format.
   *
   * @return string
   *   The new date string.
   */
  public function getDoy(string $day, string $format = 'Y-m-d'): string {
    $date = mktime(0, 0, 0, 1, $day);
    return date($format, $date);
  }

  /**
   * Helper function to get cdate.
   *
   * @param string $format_character
   *   The format character.
   *
   * @return string
   *   The date strings.
   */
  public function getCdate(string $format_character) {
    switch ($format_character) {
      case 'week':
      case 'day':
        $cdate = date('m/d/Y');
        break;

      case 'month':
        $cdate = date('n', mktime(0, 0, 0, date('m'), date('d'), date('y')));
        break;

      case 'year':
        $cdate = date('o', mktime(0, 0, 0, date('m'), date('d'), date('y')));
        break;
    }
    return $cdate;
  }

}
