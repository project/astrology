<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\astrology\Services\AstrologyUtilityService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides ability to search sign based on DOB.
 */
class AstrologyBirthSign extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'astrology_birth_sign';
  }

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Utility service object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * Class constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AstrologyCoreService $astrology_core_service,
    AstrologyUtilityService $astrology_utility_service,
  ) {
    $this->config = $config_factory;
    $this->astrologyCoreService = $astrology_core_service;
    $this->astrologyUtilityService = $astrology_utility_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('astrology.core'),
      $container->get('astrology.utility'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $value = [
      '#type' => 'date',
      '#date_date_format' => 'm/d/Y',
      '#title' => $this->t('Date of birth'),
      '#required' => TRUE,
    ];
    $form['item'] = [
      '#markup' => $this->t('Please enter your date of birth to find your astrological sign (start sign or sun sign).'),
    ];
    $form['dob'] = $value;
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Find'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $astrology_config = $this->config('astrology.settings');
    $astrology_id = $astrology_config->get('astrology');

    $dob = $form_state->getValue('dob');
    $month = date('n', $this->astrologyUtilityService->getTimestamps($dob));
    $day = date('j', $this->astrologyUtilityService->getTimestamps($dob));
    $match = FALSE;

    if ($query = $this->astrologyCoreService->getAstrologySigns($astrology_id)) {
      while ($row = $query->fetchObject()) {
        $from_date = explode('/', $row->date_range_from);
        $to_date = explode('/', $row->date_range_to);
        if ($month == $from_date[0] && $day >= $from_date[1] || $month == $to_date[0] && $day <= $to_date[1]) {
          $match = TRUE;
          $url = Url::fromRoute('astrology.astrology_birth_sign', ['sign_name' => strtolower($row->name)]);
          $form_state->setRedirectUrl($url);
        }
      }
    }
    if (!$match) {
      $this->messenger()->addError($this->t('Sorry, not able to find astrological sign for you.'));
    }
  }

}
