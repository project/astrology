<?php

namespace Drupal\astrology\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Astrology configuration settings.
 */
class AstrologyConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'astrology_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'astrology.settings',
    ];
  }

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Utility service object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->astrologyCoreService = $container->get('astrology.core');
    $instance->astrologyUtilityService = $container->get('astrology.utility');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('astrology.settings');
    $form['settings'] = [
      '#type' => 'vertical_tabs',
    ];
    // Site wide setting tab.
    $form['site'] = [
      '#type' => 'details',
      '#title' => $this->t('Site wide settings'),
      '#group' => 'settings',
      '#description' => $this->t('This setting will be available for non administrative activities like visiting sign page etc.'),
    ];
    // Administer setting tab.
    $form['administer'] = [
      '#type' => 'details',
      '#title' => $this->t('Administer settings'),
      '#group' => 'settings',
      '#description' => $this->t('Select format to performed administrative task, like to add and search text.'),
    ];

    $formats = [
      'day' => $this->t('Day'),
      'week' => $this->t('Week'),
      'month' => $this->t('Month'),
      'year' => $this->t('Year'),
    ];
    $form['site']['format_character'] = [
      '#type' => 'select',
      '#title' => $this->t('Format'),
      '#options' => $formats,
      '#default_value' => $config->get('format_character'),
    ];
    $form['administer']['admin_format_character'] = [
      '#type' => 'select',
      '#title' => $this->t('Format'),
      '#options' => $formats,
      '#default_value' => $config->get('admin_format_character'),
    ];
    $form['site']['astrology'] = [
      '#type' => 'select',
      '#title' => $this->t('Astrology'),
      '#options' => $this->astrologyCoreService->getAstrologyArray(),
      '#default_value' => $config->get('astrology'),
    ];
    $form['site']['item-check'] = [
      '#type' => 'fieldset',
      '#group' => 'site',
      '#title' => $this->t('Display sign information'),
      '#description' => $this->t('Enable to display sign information section along with text, on the sign text page.'),
    ];
    $form['site']['item-check']['sign_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sign information block'),
      '#default_value' => $config->get('sign_info'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $format_character = $form_state->getValue('format_character');
    $admin_format_character = $form_state->getValue('admin_format_character');

    $astrology = $form_state->getValue('astrology');
    $sign_info = $form_state->getValue('sign_info');

    $cdate = $this->astrologyUtilityService->getCdate($format_character);
    $admin_cdate = $this->astrologyUtilityService->getCdate($admin_format_character);

    $row = $this->astrologyCoreService->getAstrology($astrology)?->fetchObject();
    if ($row) {
      $this->astrologyCoreService->disableAllAstrology();
      $this->astrologyCoreService->enableAstrology($row->id);

      // Retrieve the configuration.
      $this->configFactory()->getEditable('astrology.settings')
        // Set the submitted configuration setting.
        ->set('format_character', $format_character)
        ->set('admin_format_character', $admin_format_character)
        ->set('astrology', $astrology)
        ->set('sign_id', '0')
        ->set('sign_info', $sign_info)
        ->set('cdate', $cdate)
        ->set('admin_cdate', $admin_cdate)
        ->save();
      $this->messenger()->addMessage($this->t('The <strong>:astrology</strong> has been set as default, and data will be shown per :format', [
        ':astrology' => $row->name,
        ':format' => $format_character,
      ]));
    }
  }

}
