<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides form to edit/update particular astrology.
 */
class AstrologyEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'astrology_edit_form';
  }

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Class constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AstrologyCoreService $astrology_core_service,
  ) {
    $this->config = $config_factory;
    $this->astrologyCoreService = $astrology_core_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('astrology.core'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $astrology_id = NULL) {
    $result = $this->astrologyCoreService->getAstrology($astrology_id);
    if (!$result) {
      throw new AccessDeniedHttpException();
    }
    $result = $result->fetchObject();
    $disabled = FALSE;
    if ($astrology_id == 1) {
      $disabled = TRUE;
    }
    $form['astrology_id'] = [
      '#type' => 'hidden',
      '#default_value' => $astrology_id,
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
      '#default_value' => $result->name,
      '#disabled' => $disabled,
    ];
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $result->enabled,
      '#disabled' => $disabled,
    ];
    $form['about'] = [
      '#type' => 'text_format',
      '#format' => $result->about_format,
      '#title' => $this->t('Description'),
      '#default_value' => $result->about,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Check if given astrology name exists.
    $astrology_name = $form_state->getValue('name');
    $astrology_id = $form_state->getValue('astrology_id');
    if ($this->astrologyCoreService->checkForDuplicateAstrologyName($astrology_name, $astrology_id)) {
      $form_state->setErrorByName('name', $this->t('Astrology name ":name" is already taken', [
        ':name' => $astrology_name,
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $name = $form_state->getValue('name');
    $enabled = $form_state->getValue('enabled');
    $about = $form_state->getValue('about');
    $astrology_id = $form_state->getValue('astrology_id');
    $updated = $this->astrologyCoreService->updateAstrology($astrology_id, [
      'name' => $name,
      'enabled' => $enabled,
      'about' => $about['value'],
      'about_format' => $about['format'],
    ]);
    if ($updated) {
      $this->astrologyCoreService->updateDefaultAstrology($astrology_id, $enabled, 'update');
      $this->messenger()->addMessage($this->t('Astrology :name updated', [':name' => $name]));
    }
  }

}
