<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\astrology\Services\AstrologyUtilityService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides ability to add text for particular sign.
 */
class AstrologySignAddTextForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'astrology_sign_add_text';
  }

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Utility service object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * The format character.
   *
   * @var string
   */
  protected string $formatCharacter;

  /**
   * Class constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AstrologyCoreService $astrology_core_service,
    AstrologyUtilityService $astrology_utility_service,
  ) {
    $this->config = $config_factory;
    $this->astrologyCoreService = $astrology_core_service;
    $this->astrologyUtilityService = $astrology_utility_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('astrology.core'),
      $container->get('astrology.utility'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $astrology_id = NULL, $sign_id = NULL) {

    $astrology_config = $this->config('astrology.settings');
    $this->formatCharacter = $astrology_config->get('admin_format_character');

    switch ($this->formatCharacter) {
      default:
      case 'day':
        $format = 'z';
        $value = [
          '#type' => 'date',
          '#date_date_format' => 'm/d/Y',
          '#title' => $this->t('Select Day'),
          '#required' => TRUE,
        ];
        break;
      case 'week':
        $format = 'W';
        $value = [
          '#type' => 'date',
          '#date_date_format' => 'm/d/Y',
          '#title' => $this->t('Select Week'),
          '#required' => TRUE,
        ];
        break;

      case 'month':
        $format = 'n';
        $value = [
          '#type' => 'select',
          '#title' => $this->t('Select Month'),
          '#options' => $this->astrologyUtilityService->getMonthsArray(),
          '#default_value' => date('n'),
        ];
        break;

      case 'year':
        $format = 'o';
        $value = [
          '#type' => 'select',
          '#title' => $this->t('Select Year'),
          '#options' => $this->astrologyUtilityService->getYearsArray(),
          '#default_value' => date('o'),
        ];
        break;
    }

    $options = $this->astrologyCoreService->getAstrologySigns($astrology_id, NULL, $sign_id);
    if (!$options) {
      throw new AccessDeniedHttpException();
    }
    $options = $options->fetchAssoc();
    $form['label'] = [
      '#type' => 'label',
      '#title' => $this->t('<strong>:name</strong>', [
        ':name' => $options['name'],
      ]),
    ];
    $form['astrology_sign_id'] = [
      '#type' => 'hidden',
      '#default_value' => $sign_id,
    ];
    $form['format_character'] = [
      '#type' => 'hidden',
      '#default_value' => $format,
    ];
    $form['date_value'] = $value;
    $form['text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Enter Text'),
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    $form['note'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Note'),
      '#description' => $this->t('You can change format ":format" setting, under administer setting tabs on the astrology configuration page  to search for other formats.', [':format' => $this->formatCharacter]),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $format_character = $form_state->getValue('format_character');
    $astrology_sign_id = $form_state->getValue('astrology_sign_id');
    $date_value = $form_state->getValue('date_value');
    $text = $form_state->getValue('text');

    switch ($format_character) {
      case 'z':
        $post_date = $this->astrologyUtilityService->getTimestamps($date_value);
        // Get day number of the year.
        $date = $this->astrologyUtilityService->getFormatDateValue('z', $date_value);
        $date_message = $this->astrologyUtilityService->getDoy($date + 1, 'l j F');
        break;

      case 'W':
        $post_date = $this->astrologyUtilityService->getTimestamps($date_value);
        // Get week number of the year.
        $date = $this->astrologyUtilityService->getFormatDateValue('W', $date_value);
        $timestamps = $this->astrologyUtilityService->getTimestamps($date_value);
        // Get first and last day of week.
        $weeks = $this->astrologyUtilityService->getFirstLastDow($timestamps);
        $date_message = date('j, M', $weeks[0]) . ' to ' . date('j, M', $weeks[1]);
        break;

      case 'n':
        $date = $date_value;
        // Get month timestamps.
        $post_date = mktime(0, 0, 0, $date_value);
        $months = $this->astrologyUtilityService->getMonthsArray();
        $date_message = $months[$date_value];
        break;

      case 'o':
        $date = $date_value;
        // Get year timestamps.
        $post_date = mktime(0, 0, 0, 1, 1, $date_value);
        $date_message = $date_value;
        break;
    }

    // Check if text for sign already exists for this format.
    $result = $this->astrologyCoreService->getAstrologicalSignText($astrology_sign_id, $date, $format_character);
    if ($result) {
      $row = $result->fetchObject();
      $this->astrologyCoreService->astrologyUpdateSignText($row->id, [
        'text' => $text['value'],
        'text_format' => $text['format'],
      ]);
      $this->messenger()->addWarning($this->t('Text updated for the :format <strong>:date</strong>.', [
        ':format' => $this->formatCharacter,
        ':date' => $date_message,
      ]));
    }
    else {
      $this->astrologyCoreService->astrologyAddSignText([
        'astrology_sign_id' => $astrology_sign_id,
        'format_character' => $format_character,
        'value' => $date,
        'text' => $text['value'],
        'text_format' => $text['format'],
        'post_date' => $post_date,
      ]);
      $this->messenger()->addMessage($this->t('Text added for the :format <strong>:date</strong>.', [
        ':format' => $this->formatCharacter,
        ':date' => $date_message,
      ]));
    }
  }

}
