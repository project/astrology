<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\astrology\Services\AstrologyUtilityService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide ability to search text for given sign.
 */
class AstrologySignTextSearch extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'astrology_list_text_config';
  }

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Utility service object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * The format character.
   *
   * @var string
   */
  protected string $formatCharacter;

  /**
   * Class constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AstrologyCoreService $astrology_core_service,
    AstrologyUtilityService $astrology_utility_service,
  ) {
    $this->config = $config_factory;
    $this->astrologyCoreService = $astrology_core_service;
    $this->astrologyUtilityService = $astrology_utility_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('astrology.core'),
      $container->get('astrology.utility'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $astrology_id = NULL) {

    $config = $this->config('astrology.settings');
    $format = $config->get('admin_format_character');
    $cdate = $config->get('admin_cdate');
    $default_sign_id = $config->get('sign_id');
    $options = $this->astrologyCoreService->getAstrologyListSignArray($astrology_id);

    switch ($format) {
      default:
      case 'day':
        $date_message = $cdate ?: date('m/d/Y');
        $value = [
          '#type' => 'date',
          '#title' => $this->t('Day'),
          '#required' => TRUE,
          '#date_date_format' => 'm/d/Y',
          '#default_value' => $cdate ?: date('m/d/Y'),
        ];
        $sign_id = [
          '#type' => 'select',
          '#title' => $this->t('Sign'),
          '#options' => $options,
          '#default_value' => $default_sign_id,
        ];
        break;
      case 'week':
        $new_date = $cdate ?: date('m/d/Y');
        $post_date = strtotime($new_date);
        $weeks = $this->astrologyUtilityService->getFirstLastDow($post_date);
        // Get week number of the year.
        $date_message = date('j, M', $weeks[0]) . ' to ' . date('j, M', $weeks[1]);

        $value = [
          '#type' => 'date',
          '#title' => $this->t('Week'),
          '#required' => TRUE,
          '#date_date_format' => 'm/d/Y',
          '#default_value' => $new_date,
        ];
        $sign_id = [
          '#type' => 'select',
          '#title' => $this->t('Sign'),
          '#options' => $options,
          '#default_value' => $default_sign_id,
        ];
        break;

      case 'month':
        $new_date = $cdate ?: date('n');
        $months = $this->astrologyUtilityService->getMonthsArray();
        $date_message = $months[$new_date];
        $value = [
          '#type' => 'select',
          '#title' => $this->t('Month'),
          '#options' => $this->astrologyUtilityService->getMonthsArray(),
          '#default_value' => $new_date,
        ];
        $sign_id = [
          '#type' => 'select',
          '#title' => $this->t('Sign'),
          '#options' => $options,
          '#default_value' => $default_sign_id,
        ];
        break;

      case 'year':
        $date_message = $cdate ?: date('o');
        $value = [
          '#type' => 'select',
          '#title' => $this->t('Year'),
          '#options' => $this->astrologyUtilityService->getYearsArray(),
          '#default_value' => $cdate ?: date('o'),
        ];
        $sign_id = [
          '#type' => 'select',
          '#title' => $this->t('Sign'),
          '#options' => $options,
          '#default_value' => $default_sign_id,
        ];
        break;
    }

    $form['fordate'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Show only items where'),
    ];
    if ($cdate) {
      $form['fordate']['item'] = [
        '#markup' => $this->t('<li> where sign <strong>:sign-name</strong> </li> <li>where :format <strong>:date</strong>.</li>', [
          ':sign-name' => array_key_exists($default_sign_id, $options) ? $options[$default_sign_id] : $options[0],
          ':format' => $format,
          ':date' => $date_message,
        ]),
      ];
    }
    $form['fordate']['sign_id'] = $sign_id;
    $form['fordate']['cdate'] = $value;
    $form['fordate']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#button_type' => 'primary',
    ];
    $form['note'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Note'),
      '#description' => $this->t('You can change format ":format" setting, under administer setting tabs on the astrology configuration page  to search for other formats.', [':format' => $format]),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $this->config->getEditable('astrology.settings')
    // Set the submitted configuration setting.
      ->set('sign_id', $form_state->getValue('sign_id'))
    // You can set multiple configurations at once by making
    // multiple calls to set()
      ->set('admin_cdate', $form_state->getValue('cdate'))->save();
  }

}
