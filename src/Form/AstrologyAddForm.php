<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\astrology\Services\AstrologyUtilityService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Astrology add form.
 */
class AstrologyAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'astrology_add_form';
  }

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Utility service object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * Class constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AstrologyCoreService $astrology_core_service,
    AstrologyUtilityService $astrology_utility_service,
  ) {
    $this->config = $config_factory;
    $this->astrologyCoreService = $astrology_core_service;
    $this->astrologyUtilityService = $astrology_utility_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('astrology.core'),
      $container->get('astrology.utility'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
    ];
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
    ];
    $form['about'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Description'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Check if astrology name exists.
    $name = $form_state->getValue('name');
    if ($this->astrologyCoreService->checkForDuplicateAstrologyName($name)) {
      $form_state->setErrorByName('name', $this->t('Astrology name ":name" is already taken', [':name' => $name]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $name = $form_state->getValue('name');
    $enabled = $form_state->getValue('enabled');
    $about = $form_state->getValue('about');
    $astrology_id = $this->astrologyCoreService->addAstrology([
      'name' => $name,
      'enabled' => $enabled,
      'about' => $about['value'],
      'about_format' => $about['format'],
    ]);

    if ($enabled) {
      $this->astrologyCoreService->updateDefaultAstrology($astrology_id, $enabled, 'new');
    }
    $form_state->setRedirect('astrology.list_astrology');
    $this->messenger()->addMessage($this->t('Astrology :name created', [':name' => $name]));
  }

}
