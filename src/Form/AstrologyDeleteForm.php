<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AstrologyDeleteSignForm.
 */
class AstrologyDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'astrology_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this astrology?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('astrology.list_astrology');
  }

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Astrology ID.
   *
   * @var string
   */
  protected string $astrologyId;

  /**
   * Class constructor.
   */
  public function __construct(AstrologyCoreService $astrology_core_service) {
    $this->astrologyCoreService = $astrology_core_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('astrology.core'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $astrology_id = NULL) {
    if ($astrology_id == 1) {
      $this->messenger()->addError($this->t("This astrology can not be deleted"));
      throw new AccessDeniedHttpException();
    }
    $form['#title'] = $this->getQuestion();
    $this->astrologyId = $astrology_id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $row = $this->astrologyCoreService->getAstrology($this->astrologyId)?->fetchObject();
    if ($row) {
      $this->astrologyCoreService->deleteAstrology($this->astrologyId);
      if ($row->enabled) {
        $this->astrologyCoreService->updateDefaultAstrology($this->astrologyId, $row->enabled, 'delete');
      }
      $this->messenger()->addMessage($this->t("Astrology %name deleted.", ['%name' => $row->name]));
      $form_state->setRedirect('astrology.list_astrology');
    }
  }

}
