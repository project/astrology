<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides ability to edit text for sign.
 */
class AstrologySignEditTextForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'astrology_sign_edit_text';
  }

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Class constructor.
   */
  public function __construct(AstrologyCoreService $astrology_core_service) {
    $this->astrologyCoreService = $astrology_core_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('astrology.core'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $astrology_id = NULL, $sign_id = NULL, $text_id = NULL) {

    $sign = $this->astrologyCoreService->getAstrologySigns($astrology_id, NULL, $sign_id)?->fetchObject();
    $result = $this->astrologyCoreService->isValidText($sign_id, $text_id);
    $form['label'] = [
      '#markup' => $this->t('Name <strong>:name</strong>', [
        ':name' => $sign?->name,
      ]),
    ];
    $form['astrology_id'] = [
      '#type' => 'hidden',
      '#default_value' => $astrology_id,
    ];
    $form['text_id'] = [
      '#type' => 'hidden',
      '#default_value' => $text_id,
    ];
    $form['text'] = [
      '#type' => 'text_format',
      '#format' => $result->text_format,
      '#title' => $this->t('Enter Text'),
      '#required' => TRUE,
      '#default_value' => $result->text,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $text = $form_state->getValue('text');
    $text_id = $form_state->getValue('text_id');
    $astrology_id = $form_state->getValue('astrology_id');
    $data = [
      'text' => $text['value'],
      'text_format' => $text['format'],
    ];
    if ($this->astrologyCoreService->updateText($data, $text_id)) {
      $form_state->setRedirect('astrology.astrology_sign_list_text', ['astrology_id' => $astrology_id]);
      $this->messenger()->addMessage($this->t('Text updated.'));
    }
  }

}
