<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\astrology\Services\AstrologyUtilityService;
use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides form to delete specific sign of particular astrology.
 */
class AstrologyDeleteSignForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'astrology_delete_sign';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this sign?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('astrology.list_astrology_sign', ['astrology_id' => $this->astrologyId]);
  }

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Utility service object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * The astrology ID.
   *
   * @var string
   */
  protected string $astrologyId;

  /**
   * The sign ID.
   *
   * @var string
   */
  protected string $signId;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $cacheTagsInvalidator;

  /**
   * Class constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AstrologyCoreService $astrology_core_service,
    AstrologyUtilityService $astrology_utility_service,
    CacheTagsInvalidator $cache_tags_invalidator,
  ) {
    $this->config = $config_factory;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->astrologyCoreService = $astrology_core_service;
    $this->astrologyUtilityService = $astrology_utility_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('astrology.core'),
      $container->get('astrology.utility'),
      $container->get('cache_tags.invalidator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $astrology_id = NULL, $sign_id = NULL) {

    $signs = $this->astrologyCoreService->getAstrologySigns($astrology_id, NULL, $sign_id)?->fetchAssoc();
    if ($astrology_id == 1 || ($signs && $signs['astrology_id'] !== $astrology_id)) {
      $this->messenger()->addError($this->t("This sign can not be deleted"));
      throw new AccessDeniedHttpException();
    }
    $form['#title'] = $this->getQuestion();
    $this->astrologyId = $astrology_id;
    $this->signId = $sign_id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->astrologyCoreService->deleteSign($this->astrologyId, $this->signId);
    $form_state->setRedirect('astrology.list_astrology_sign', ['astrology_id' => $this->astrologyId]);
    $this->messenger()->addMessage($this->t("Sing deleted."));
    $this->cacheTagsInvalidator->invalidateTags(['astrology_block']);
  }

}
