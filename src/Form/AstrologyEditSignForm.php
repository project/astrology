<?php

namespace Drupal\astrology\Form;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\astrology\Services\AstrologyUtilityService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AstrologyAddSignForm.
 */
class AstrologyEditSignForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'astrology_edit_sign';
  }

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * Utility service object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * Class constructor.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    AstrologyCoreService $astrology_core_service,
    AstrologyUtilityService $astrology_utility_service,
  ) {
    $this->config = $config_factory;
    $this->fileSystem = $file_system;
    $this->astrologyCoreService = $astrology_core_service;
    $this->astrologyUtilityService = $astrology_utility_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('astrology.core'),
      $container->get('astrology.utility'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $astrology_id = NULL, $sign_id = NULL) {

    $astrology_signs = $this->astrologyCoreService->getAstrologySigns($astrology_id, NULL, $sign_id);
    if (!$astrology_signs) {
      throw new AccessDeniedHttpException();
    }
    $astrology_signs = $astrology_signs->fetchAssoc();
    $date_range_from = explode('/', $astrology_signs['date_range_from']);
    $date_range_to = explode('/', $astrology_signs['date_range_to']);

    $is_disabled = FALSE;
    if ($astrology_id == 1) {
      $is_disabled = TRUE;
    }

    $form['astrology_id'] = [
      '#type' => 'hidden',
      '#default_value' => $astrology_id,
    ];
    $form['sign_id'] = [
      '#type' => 'hidden',
      '#default_value' => $sign_id,
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $astrology_signs['name'],
      '#disabled' => $is_disabled,
      '#required' => TRUE,
    ];
    $form['icon'] = [
      '#type' => 'file',
      '#title' => $this->t('icon'),
    ];
    $form['date_range'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Date range value'),
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    ];
    $form['date_range']['date_range_from'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('From date'),

    ];
    $form['date_range']['date_range_to'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('To date'),
    ];
    $form['date_range']['date_range_from']['from_date_month'] = [
      '#type' => 'select',
      '#title' => $this->t('Month'),
      '#options' => $this->astrologyUtilityService->getMonthsArray(),
      '#default_value' => $date_range_from[0],
      '#disabled' => $is_disabled,
      '#required' => TRUE,
    ];
    $form['date_range']['date_range_from']['from_date_day'] = [
      '#type' => 'select',
      '#title' => $this->t('Day'),
      '#options' => $this->astrologyUtilityService->getDaysArray(),
      '#default_value' => $date_range_from[1],
      '#disabled' => $is_disabled,
      '#required' => TRUE,
    ];
    $form['date_range']['date_range_to']['to_date_month'] = [
      '#type' => 'select',
      '#title' => $this->t('Month'),
      '#options' => $this->astrologyUtilityService->getMonthsArray(),
      '#default_value' => $date_range_to[0],
      '#disabled' => $is_disabled,
      '#required' => TRUE,
    ];
    $form['date_range']['date_range_to']['to_date_day'] = [
      '#type' => 'select',
      '#title' => $this->t('Day'),
      '#options' => $this->astrologyUtilityService->getDaysArray(),
      '#default_value' => $date_range_to[1],
      '#disabled' => $is_disabled,
      '#required' => TRUE,
    ];
    $form['about'] = [
      '#type' => 'text_format',
      '#format' => $astrology_signs['about_sign_format'],
      '#title' => $this->t('Description'),
      '#default_value' => $astrology_signs['about_sign'],
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    if ($is_disabled) {
      $form['note'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Note'),
        '#description' => $this->t('Sign <strong>:sign</strong> belongs to the default astrology zodiac, hence you are only allowed to edit few information.', [':sign' => $astrology_signs['name']]),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Check if astrology sign name exists.
    $sign_name = $form_state->getValue('name');
    $sign_id = $form_state->getValue('sign_id');
    if ($this->astrologyCoreService->checkForDuplicateSignName($sign_name, $sign_id)) {
      $form_state->setErrorByName('name', $this->t('Sign name ":name" is already taken', [':name' => $sign_name]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $from_date = $form_state->getValue('from_date_month');
    $about_sign = $form_state->getValue('about');
    $from_date .= '/' . $form_state->getValue('from_date_day');
    $to_date = $form_state->getValue('to_date_month');
    $to_date .= '/' . $form_state->getValue('to_date_day');

    $file_path = 'public://astrology/image';
    $validators = [];
    $data = [
      'name' => $form_state->getValue('name'),
      'date_range_from' => $from_date,
      'date_range_to' => $to_date,
      'about_sign' => $about_sign['value'],
      'about_sign_format' => $about_sign['format'],
    ];
    $this->fileSystem->prepareDirectory($file_path, FileSystemInterface::CREATE_DIRECTORY);
    if (version_compare(\Drupal::VERSION, '10.3.0', '>=')) {
      $fileExists = FileExists::Replace;
    }
    else {
      // @phpstan-ignore-next-line
      $fileExists = FileSystemInterface::EXISTS_REPLACE;
    }
    if ($file = file_save_upload('icon', $validators, $file_path, 0, $fileExists)) {
      $file->setPermanent();
      $data['icon'] = $file->getFileUri();
      $this->astrologyCoreService->updateAstrologySign($data, $form_state->getValue('sign_id'), $form_state->getValue('astrology_id'));
      $file->save();
    }
    else {
      $this->astrologyCoreService->updateAstrologySign($data, $form_state->getValue('sign_id'), $form_state->getValue('astrology_id'));
    }
    $form_state->setRedirect('astrology.list_astrology_sign', ['astrology_id' => $form_state->getValue('astrology_id')]);
    $this->messenger()->addMessage($this->t('Sign :name updated.', [':name' => $form_state->getValue('name')]));
  }

}
