<?php

namespace Drupal\astrology\Controller;

use Drupal\astrology\Services\AstrologyCoreService;
use Drupal\astrology\Services\AstrologyUtilityService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * The astrology controller class.
 */
class AstrologyController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Drupal\Core\Routing\RedirectDestinationInterface.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectService;

  /**
   * Drupal\astrology\Services\AstrologyCoreService.
   *
   * @var \Drupal\astrology\Services\AstrologyCoreService
   */
  protected $astrologyCoreService;

  /**
   * File url generator object.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * Utility service object.
   *
   * @var \Drupal\astrology\Services\AstrologyUtilityService
   */
  protected $astrologyUtilityService;

  /**
   * Class constructor.
   */
  public function __construct(
    $formBuilder,
    ConfigFactoryInterface $configFactory,
    AstrologyCoreService $astrology_core_service,
    RedirectDestinationInterface $redirectService,
    FileUrlGenerator $fileUrlGenerator,
    AstrologyUtilityService $astrology_utility_service,
  ) {
    $this->config = $configFactory;
    $this->formBuilder = $formBuilder;
    $this->redirectService = $redirectService;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->astrologyCoreService = $astrology_core_service;
    $this->astrologyUtilityService = $astrology_utility_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('config.factory'),
      $container->get('astrology.core'),
      $container->get('redirect.destination'),
      $container->get('file_url_generator'),
      $container->get('astrology.utility'),
    );
  }

  /**
   * Astrological sign page for given date of birth.
   *
   * @param string|null $sign_name
   *   The sign name to list page and its text.
   */
  public function astrologicalSignPage(string $sign_name = NULL): array {

    $astrology_config = $this->config('astrology.settings');
    $astrology_id = $astrology_config->get('astrology');
    $formatter = $astrology_config->get('format_character');
    $sign = $this->astrologyCoreService
      ->getAstrologySigns($astrology_id, $sign_name)
      ->fetchObject();
    if (!$sign) {
      throw new NotFoundHttpException();
    }
    $about_sign_summary = text_summary($sign->about_sign, $sign->about_sign_format);
    $from_date = explode('/', $sign->date_range_from);
    $to_date = explode('/', $sign->date_range_to);
    $from_month = $from_date[0];
    $from_day = $from_date[1];
    $to_month = $to_date[0];
    $today = $to_date[1];
    $date_range_sign = date('M, j', mktime(0, 0, 0, $from_month, $from_day));
    $date_range_sign .= ' - ' . date('M, j', mktime(0, 0, 0, $to_month, $today));
    $build[] = [
      '#theme' => 'astrology-dob-sign',
      '#sign' => $sign,
      '#formatter' => $formatter,
      '#date_range_sign' => $date_range_sign,
      '#about_sign_summary' => $about_sign_summary,
    ];
    $build['#title'] = $this->t('Your astrological sign is ":sign"', [':sign' => $sign_name]);
    return $build;
  }

  /**
   * Astrology sign details page.
   *
   * @param string|null $sign_name
   *   Sign name.
   */
  public function astrologySignDetailsPage(string $sign_name = NULL): array {

    $astrology_config = $this->config('astrology.settings');
    $astrology_id = $astrology_config->get('astrology');
    $sign = $this->astrologyCoreService
      ->getAstrologySigns($astrology_id, $sign_name)
      ->fetchObject();
    if (!$sign) {
      // Throw new AccessDeniedHttpException();
      throw new NotFoundHttpException();
    }
    return [
      '#theme' => 'astrology-sign-text',
      '#sign' => $sign,
    ];
  }

  /**
   * Displaying text for sign according to the format selected.
   *
   * @param string|null $sign_name
   *   Aries, Taurus etc.
   * @param string|null $formatter
   *   Day, week etc.
   * @param string|null $next_prev
   *   Day number, week number, month number etc, default is 0.
   */
  public function astrologyListTextSignPage(string $sign_name = NULL, string $formatter = NULL, string $next_prev = NULL): array {

    $astrology_config = $this->config('astrology.settings');
    $astrology_id = $astrology_config->get('astrology');
    $sign_info = $astrology_config->get('sign_info');

    $format_char = $formatter;
    $allowed_format = ['day', 'week', 'month', 'year'];
    $sign = $this->astrologyCoreService->getAstrologySigns($astrology_id, $sign_name)->fetchObject();
    if (!$sign || !in_array($formatter, $allowed_format)) {
      throw new NotFoundHttpException();
    }
    if ($next_prev && !$this->astrologyUtilityService->astrologyCheckValidDate($formatter, $next_prev)) {
      throw new NotFoundHttpException();
    }

    $about_sign_summary = text_summary($sign->about_sign, $sign->about_sign_format);
    $from_date = explode('/', $sign->date_range_from);
    $to_date = explode('/', $sign->date_range_to);
    $from_month = $from_date[0];
    $from_day = $from_date[1];
    $to_month = $to_date[0];
    $today = $to_date[1];

    // Default date & post date value.
    $date = date('z');
    $post_date = mktime(0, 0, 0, 1, ($date + 1), date('o'));
    switch ($format_char) {
      case 'day':
        $format = 'z';
        $date = $next_prev ?: date('z');
        $post_date = mktime(0, 0, 0, 1, ($date + 1), date('o'));
        $date_format = 'l, j F';
        break;

      case 'week':
        $format = 'W';
        $date = $next_prev ?: date('W');
        $post_date = mktime(0, 0, 0, 1, (4 + 7 * ($date - 1)), date('o'));
        $date_format = 'j, M';
        break;

      case 'month':
        $format = 'n';
        $date = $next_prev ?: date('n');
        $post_date = mktime(0, 0, 0, $date);
        $date_format = 'F';
        break;

      case 'year':
        $format = 'o';
        $date = $next_prev ?: date('o');
        $date_format = 'Y';
        $post_date = mktime(0, 0, 0, 1, 1, $date);
        break;
    }

    $astrology_text = $this->astrologyCoreService->getAstrologicalSignText($sign->id, $date, $format);
    if (!$next_prev) {
      $next_prev = $date;
    }
    $next_prev_val = $this->astrologyUtilityService->astrologyCheckNextPrev($formatter, $next_prev);
    $weeks = $this->astrologyUtilityService->getFirstLastDow($post_date);
    $date_range_sign = date('M, j', mktime(0, 0, 0, $from_month, $from_day));
    $date_range_sign .= ' - ' . date('M, j', mktime(0, 0, 0, $to_month, $today));

    $build[] = [
      '#theme' => 'astrology-text',
      '#sign' => $sign,
      '#sign_info' => $sign_info,
      '#formatter' => $formatter,
      '#date_format' => $date_format,
      '#astrology_text' => $astrology_text ? $astrology_text->fetchObject() : '',
      '#about_sign_summary' => $about_sign_summary,
      '#post_date' => $post_date,
      '#weeks' => $weeks,
      '#for_date' => $next_prev_val,
      '#date_range_sign' => $date_range_sign,
    ];
    if ($format_char == 'day') {
      $title = $this->t('Astrology of the day');
    }
    else {
      $title = $this->t('Astrology for the :date', [':date' => $format_char]);
    }
    $build['#title'] = $title;
    return $build;
  }

  /**
   * Search text for available signs from selected astrology.
   *
   * @param int|null $astrology_id
   *   Astrology id.
   */
  public function astrologySignTextSearch(int $astrology_id = NULL): array {

    $build['config_data'] = $this->formBuilder->getForm('Drupal\astrology\Form\AstrologySignTextSearch', $astrology_id);

    $astrology_config = $this->config('astrology.settings');
    $formatter = $astrology_config->get('admin_format_character');
    $cdate = $astrology_config->get('admin_cdate');
    $astrology_sign = $astrology_config->get('sign_id');

    $build['config_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Icon'),
        $this->t('Name'),
        $this->t('Text'),
        $this->t(':dated', [':dated' => $formatter]),
        $this->t('Operations'),
      ],
      '#sticky' => TRUE,
      '#empty' => $this->t('There are no items to display.'),
    ];

    switch ($formatter) {
      case 'day':
        $format = 'z';
        $timestamp = $this->astrologyUtilityService
          ->getFormatDateValue('z', $cdate ?: date('m/d/Y'));
        break;

      case 'week':
        $format = 'W';
        $timestamp = $this->astrologyUtilityService
          ->getFormatDateValue('W', $cdate ?: date('m/d/Y'));
        break;

      case 'month':
        $format = 'n';
        $timestamp = $cdate ?: date('n', mktime(0, 0, 0, date("m"), date("d")));
        $month = date('F, Y', mktime(0, 0, 0, (int) $timestamp, date('d'), date('y')));
        break;

      case 'year':
        $format = 'o';
        $timestamp = $cdate ?: date('o', mktime(0, 0, 0, date("m"), date("d")));
        break;
    }

    if ($astrology_sign) {
      $result = $this->astrologyCoreService
        ->getAllTextForSign($astrology_id, $astrology_sign, $format, $timestamp);
    }
    else {
      $result = $this->astrologyCoreService
        ->getAllTextForAllSigns($astrology_id, $format, $timestamp);
    }
    foreach ($result as $row) {
      $weeks = $this->astrologyUtilityService->getFirstLastDow($row->post_date);
      $icon = $this->t('<img src=":src" alt=":alt" height=":height" width=":width" />', [
        ':src' => $this->fileUrlGenerator->generateAbsoluteString($row->icon),
        ':alt' => $row->name,
        ':height' => '30',
        ':width' => '30',
      ]);
      $week = date('j, M', $weeks[0]) . ' - ' . date('j, M', $weeks[1]);
      $dated = ($formatter == 'day') ? date('j M,Y', $row->post_date) :
       (($formatter == 'week') ? $week :
        (($formatter == 'month') ? $month : $timestamp));

      $build['config_table'][$row->astrology_sign_id]['icon'] = [
        '#markup' => $icon,
      ];
      $build['config_table'][$row->astrology_sign_id]['name'] = [
        '#markup' => $row->name,
      ];
      $build['config_table'][$row->astrology_sign_id]['text'] = [
        '#markup' => text_summary($row->text, $row->text_format),
      ];
      $build['config_table'][$row->astrology_sign_id]['dated'] = [
        '#plain_text' => $dated,
      ];

      // Operations (drop down button) column.
      $build['config_table'][$row->astrology_sign_id]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];
      $build['config_table'][$row->astrology_sign_id]['operations']['#links']['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('astrology.astrology_sign_text_edit', [
          'astrology_id' => $astrology_id,
          'sign_id' => $row->astrology_sign_id,
          'text_id' => $row->id,
        ]),
      ];
    }
    return $build;
  }

  /**
   * List all signs from available astrology.
   *
   * @param int|null $astrology_id
   *   The node to add banners to.
   */
  public function astrologyListSign(int $astrology_id = NULL): array {

    $build['config_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Icon'),
        $this->t('Name'),
        $this->t('Description'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('There are no signs available in this astrology.'),
    ];

    $result = $this->astrologyCoreService->getAstrologySigns($astrology_id) ?? [];
    $destination = $this->redirectService->getAsArray();

    foreach ($result as $row) {
      $icon = $this->t('<img src=":src" alt=":alt" height=":height" width=":width" />', [
        ':src' => $this->fileUrlGenerator->generateAbsoluteString($row->icon),
        ':alt' => $row->name,
        ':height' => '30',
        ':width' => '30',
      ]);
      $build['config_table'][$row->id]['icon'] = [
        '#markup' => $icon,
      ];
      $build['config_table'][$row->id]['name'] = [
        '#plain_text' => $row->name,
      ];
      $build['config_table'][$row->id]['about_sign'] = [
        '#markup' => text_summary($row->about_sign, $row->about_sign_format),
      ];

      // Operations (dropdown button) column.
      $build['config_table'][$row->id]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];
      $build['config_table'][$row->id]['operations']['#links']['edit_sign'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('astrology.edit_astrology_sign', [
          'astrology_id' => $row->astrology_id,
          'sign_id' => $row->id,
        ])->setOption('query', $destination),
      ];
      $build['config_table'][$row->id]['operations']['#links']['add_text'] = [
        'title' => $this->t('Add text'),
        'url' => Url::fromRoute('astrology.add_text_astrology_sign', [
          'astrology_id' => $row->astrology_id,
          'sign_id' => $row->id,
        ]),
      ];

      // Remove delete option for astrology signs from zodiac.
      if ($row->astrology_id != 1) {
        $build['config_table'][$row->id]['operations']['#links']['delete_sign'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('astrology.delete_astrology_sign', [
            'astrology_id' => $row->astrology_id,
            'sign_id' => $row->id,
          ]),
        ];
      }
    }
    return $build;
  }

  /**
   * Configuration page that shows available astrology and its default setting.
   */
  public function astrologyConfig(): array {

    $build['config_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Description'),
        $this->t('Enabled'),
        $this->t('Operations'),
      ],
    ];

    $result = $this->astrologyCoreService->getAllAstrology();
    $destination = $this->redirectService->getAsArray();
    foreach ($result as $row) {
      $build['config_table'][$row->id]['name'] = [
        '#markup' => $this->t('<a href="@url">:name</a>', [
          ':name' => $row->name,
          '@url' => Url::fromRoute('astrology.list_astrology_sign', ['astrology_id' => $row->id])->toString(),
        ]),
      ];
      $build['config_table'][$row->id]['about'] = [
        '#markup' => text_summary($row->about, $row->about_format),
      ];
      $build['config_table'][$row->id]['enabled'] = [
        '#markup' => ($row->enabled) ? '<strong>Yes</strong>' : 'No',
      ];
      $build['config_table'][$row->id]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];
      $build['config_table'][$row->id]['operations']['#links']['edit_astrology'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('astrology.edit_astrology', [
          'astrology_id' => $row->id,
        ])->setOption('query', $destination),
      ];
      $build['config_table'][$row->id]['operations']['#links']['list_sign'] = [
        'title' => $this->t('Signs'),
        'url' => Url::fromRoute('astrology.list_astrology_sign', ['astrology_id' => $row->id]),
      ];
      $build['config_table'][$row->id]['operations']['#links']['list_text'] = [
        'title' => $this->t('Text'),
        'url' => Url::fromRoute('astrology.astrology_sign_list_text', ['astrology_id' => $row->id]),
      ];
      // Only super admin can delete default astrology zodiac.
      if ($row->id != 1) {
        $build['config_table'][$row->id]['operations']['#links']['delete_astrology'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('astrology.delete_astrology', ['astrology_id' => $row->id]),
        ];
      }
    }
    $build['config_data'] = $this->formBuilder->getForm('Drupal\astrology\Form\AstrologyConfig');
    return $build;
  }

}
